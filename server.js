// install express server

// and listen to port 3000
// and answer to get request with index.html
const express = require('express');
const app = express();
const PORT = 3000
const fetch = require("node-fetch"); 
require('dotenv').config();
const API_KEY = process.env.API_KEY;

app.get("/", function (req, res) {
    // answer with index.html
    console.log(req.headers["x-forwarded-for"])
    res.sendFile(__dirname + "/index.html");
});

// add endpoint for get request to /api
app.get("/api", function (req, res) {
    // answer with hello world html
    console.log(req.path);
    console.log(req.query);
    res.send("<h1>Hello World</h1>");
});

app.get("/hello", function (req, res) {
    // answer with hello world html
	console.log(req.params);
    console.log(req.path);
    console.log(req.query.name ? req.query.name : "No query params");
    res.send(`<h1>Hello ${req.query.name ? req.query.name : "Anonymous"}</h1>`);
});

app.get("/whatismyip", async function (req, res) {
    // answer with hello world html
    try {
        //const response = await fetch(`https://ipapi.co/${req.headers["x-forwarded-for"]}/json/`);
        //const response = await fetch("https://api.ipify.org?format=json", {
        const response = await fetch(`http://api.ipapi.com/api/${req.headers["x-forwarded-for"]}?access_key=${API_KEY}`);
        // get location from ip
        const data = await response.json();
	console.log(data);
        const city = data.city;
        const country = data.country_name;
        const region = data.region_name;
        res.send(`<h1>Your IP is ${req.headers["x-forwarded-for"]}<br>${country}<br>${region}<br>${city}</h1>`);
    } catch (error) {
        console.log(error);
        res.send(`<h1>Something went wrong</h1>`);
    }

});
app.listen(PORT);

